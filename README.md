Reverse engineered schematic for a work lamp with movable arm and magnifying glass, Biltema art. no. 35-9813. The lamp uses a
circular fluorescent bulb, type 22W G10q.
The electronic circuit had failed, so not all component values are known. Capacitor C1 is a 
[Class-X safety capacitor](https://www.allaboutcircuits.com/technical-articles/safety-capacitor-class-x-and-class-y-capacitors/).